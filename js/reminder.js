const adapter = new LocalStorage('db');
const db = low(adapter);
let translateTo = 'English';
chrome.storage.local.get(['translateTo'], function(result) {
    translateTo = result.translateTo;
});

function getRemindWords() {
    return db.get('remindPool').value();
}

$(document).ready(function() {
    const remindWords = getRemindWords();
    const appendPosition = $('.body_wrap > ul');
    
    if (remindWords) {
        const len = remindWords.length;
        for (let i=0; i<len; i++) {
            const remindWord = translateTo === 'English' ? remindWords[i].eng : remindWords[i].kor[0];
            const wordClass = remindWords[i].class;
            const meaning = translateTo === 'English' ? remindWords[i].orikor : remindWords[i].eng;
            const template = 
            `<li class="remind_item">
                <div class="remind_word">
                    <span>${remindWord}</span>
                </div>
                <div class="remind_infos">
                    <span class="wordClass">[${wordClass}]</span>
                    <span class="wordMeaning">[${meaning}]</span>
                </div>
            </li>`;
            appendPosition.append(template);
        }
        $('.remind_word').click(function(ev) {
            ev.stopPropagation();
            $(ev.currentTarget).parent().find('.remind_infos').toggleClass('active');
        });
    }
});