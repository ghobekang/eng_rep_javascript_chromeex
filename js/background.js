const adapter = new LocalStorage("db");
const db = low(adapter);

let fireStorage, fireStore;
let isNaverblog = false;
let isPauseStatus = false;
let unknownWords = [];
let knownWords = [];
let exceptions = [
  /www\.youtube\.com/,
  /endic\.naver\.com/,
  /jp\.dict\.naver\.com/,
  /www\.naver\.com/,
  /console\.firebase\.google\.com/,
  /nid\.naver\.com/,
  /mail\.naver\.com/,
  /map\.naver\.com/,
  /map\.daum\.net/,
  /map\.google\.com/,
  /search\.naver\.com/,
  /www\.google\.com\/search/,
  /search\.daum\.net/,
  /docs\.google\.com/,
  /mail\.google\.com/,
  /console\./
];
let translateTo = "English";
let currentLv = "Basic";
let limitWords = 30;
let licenseStatus;
let enableLicence;
let revisionNum = 1;

const config = {
  apiKey: "AIzaSyB9x3-Lc3Yj48o7u7PSDHAp_ID4PF0H4YA",
  authDomain: "osori-4515b.firebaseapp.com",
  projectId: "osori-4515b",
  storageBucket: "gs://osori-4515b.appspot.com/"
};
firebase.initializeApp(config);

init_database();
chrome.storage.local.set({
  translateTo: "English",
  isPaused: false,
  currentLevel: currentLv,
  wordLimit: limitWords
});
window.localStorage.setItem("exceptions", exceptions);

const transferWordsToContentScript = function() {
  const remindWords = db.get("remindPool").value();
  const unknown = _.shuffle(unknownWords);
  const knownWords = db.get("known").value();
  const commonSentences = db.get("common").value();

  sendWordsToContentsScript(
    unknown,
    knownWords,
    commonSentences,
    remindWords,
    limitWords
  );

  return false;
};

const getRegexr = function(bunchofWords, flag, callback) {
  if (bunchofWords.length < 1) {
    return false;
  }
  let regkor = [];
  const tailPatterns_i = "(?=[^가-힣])";
  const tailPatterns_a =
    "(?=(는[\\.\\?\\!]|다[\\.\\?\\!]|는다[\\.\\?\\!]|군[\\.\\?\\!]|는군[\\.\\?\\!]|네[\\.\\?\\!]|마[\\.\\?\\!]|걸[\\.\\?\\!]|래[\\.\\?\\!]|라[\\.\\?\\!]|는단다[\\.\\?\\!]|단다[\\.\\?\\!]|란다[\\.\\?\\!]|냐\\?|느냐\\?|니\\?|련\\?|을쏘냐\\?|쏘냐\\?|대\\?|담\\?|려무나[\\.\\?\\!]|렴[\\.\\?\\!]|소서[\\.\\?\\!]|요[\\.\\?\\!]|니다[\\.\\?\\!]|습니다[\\.\\?\\!]|니까[\\.\\?\\!]|습니까[\\.\\?\\!]|겠다[\\.\\?\\!]|겠어[\\.\\?\\!]|겠어요[\\.\\?\\!]|겠습니다[\\.\\?\\!]|서|여서|니|니까|지만|데|더니|고|면|야|여야|시다[\\.\\?\\!]|셔[\\.\\?\\!]|세요[\\.\\?\\!]|셔요[\\.\\?\\!]|십니다[\\.\\?\\!]|셨다[\\.\\?\\!]|셨어[\\.\\?\\!]|셨어요[\\.\\?\\!]|셨습니다[\\.\\?\\!]|시느냐[\\.\\?\\!]|십니까[\\.\\?\\!]|셨느냐[\\.\\?\\!]|셨습니까[\\.\\?\\!]|시겠다[\\.\\?\\!]|시겠어[\\.\\?\\!]|시겠어요[\\.\\?\\!]|시겠습니다[\\.\\?\\!]|셔서|시니|시니까|시지만|신데|시더니|시고|시면|셔야)[^가-힣])(?!,)";
  const tailPatterns_v =
    "(?=(다[\\.\\?\\!]|는다[\\.\\?\\!]|군[\\.\\?\\!]|는군[\\.\\?\\!]|네[\\.\\?\\!]|마[\\.\\?\\!]|걸[\\.\\?\\!]|래[\\.\\?\\!]|라[\\.\\?\\!]|는단다[\\.\\?\\!]|단다[\\.\\?\\!]|란다[\\.\\?\\!]|냐\\?|느냐\\?|니\\?|련\\?|을쏘냐\\?|쏘냐\\?|대\\?|담\\?|려무나[\\.\\?\\!]|렴[\\.\\?\\!]|소서[\\.\\?\\!]|요[\\.\\?\\!]|니다[\\.\\?\\!]|니까[\\.\\?\\!]|자[\\.\\?\\!]|시다[\\.\\?\\!]|여라[\\.\\?\\!]|시오[\\.\\?\\!]|서[\\.\\?\\!]|겠다[\\.\\?\\!]|겠어[\\.\\?\\!]|겠어요[\\.\\?\\!]|겠습니다[\\.\\?\\!]|여서|니|니까|지만|는데|더니|고|면|야|여야|려고|습니다[\\.\\?\\!]|습니까\\?|느냐[\\.\\?\\!]|요[\\.\\?\\!]|신다[\\.\\?\\!]|셔[\\.\\?\\!]|세요[\\.\\?\\!]|셔요[\\.\\?\\!]|십니다[\\.\\?\\!]|셨다[\\.\\?\\!]|셨어[\\.\\?\\!]|셨어요[\\.\\?\\!]|셨습니다[\\.\\?\\!]|시느냐[\\.\\?\\!]|십니까\\?|셨느냐[\\.\\?\\!]|셨습니까\\?|시라[\\.\\?\\!]|십시오[\\.\\?\\!]|시겠다[\\.\\?\\!]|시겠어[\\.\\?\\!]|시겠어요[\\.\\?\\!]|시겠습니다[\\.\\?\\!]|셔서|시니|시니까|시지만|시는데|시더니|시고|시면|셔야|시려고)[^가-힣])(?!,)";
  const tailPatterns_vp =
    "?(?=(다[\\.\\?\\!]|어[\\.\\?\\!]|어요[\\.\\?\\!]|습니다[\\.\\?\\!]|느냐[\\.\\?\\!]|습니까[\\.\\?\\!]|셨다[\\.\\?\\!]|셨어[\\.\\?\\!]|셨어요[\\.\\?\\!]|셨습니다[\\.\\?\\!]|셨느냐[\\.\\?\\!]|셨습니까[\\.\\?\\!]|을라[\\.\\?\\!]|단다[\\.\\?\\!]|을걸[\\.\\?\\!]|군[\\.\\?\\!]|구나[\\.\\?\\!]|란다[\\.\\?\\!]|느냐[\\.\\?\\!]|니[\\.\\?\\!]|련[\\.\\?\\!]|으랴[\\.\\?\\!]|을쏘냐[\\.\\?\\!]|대[\\.\\?\\!]|담[\\.\\?\\!]|으니까|으면서|으면)[^가-힣])(?!,)";
  const tailPatterns_n =
    "(?=(이|가|로써|으로써|에서|에게서|부터|까지|에게|한테|께|와|과|에서|을|를|의|로서|으로서|으로|로)[^가-힣])(?!,)";
  const tailPatterns_d = tailPatterns_i;
  const tailPatterns_t = tailPatterns_v;

  for (let i = 0; i < bunchofWords.length; i++) {
    if (bunchofWords[i] === "") {
      return false;
    }
    switch (flag) {
      case "i": {
        regkor[i] =
          "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_i;
        break;
      }
      case "a": {
        regkor[i] =
          "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_a;
        break;
      }
      case "v": {
        regkor[i] =
          "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_v;
        break;
      }
      case "vp": {
        regkor[i] =
          "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_vp;
        break;
      }
      case "n": {
        regkor[i] =
          "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_n;
        break;
      }
      case "d": {
        regkor[i] = bunchofWords[i] + tailPatterns_d;
        break;
      }
      case "t": {
        regkor[i] = bunchofWords[i] + tailPatterns_t;
        break;
      }
    }
  }
  return regkor;
};

const sendWordsToContentsScript = function(
  words,
  knownWords,
  common,
  remindwords,
  limit
) {
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      {
        regexrWords: words,
        known: knownWords,
        common: common,
        reminds: remindwords,
        isNaverBlog: isNaverblog,
        trans: translateTo,
        limit: limit
      },
      function(response) {
        return false;
      }
    );
  });
};

const injectScriptsToContentsScript = function() {
  chrome.tabs.executeScript(
    {
      file: "/node_modules/jquery/dist/jquery.min.js"
    },
    function() {
      chrome.tabs.executeScript(
        {
          file: "/js/external/jquery.toolbar.js"
        },
        function() {
          chrome.tabs.insertCSS({
            file: "/js/external/jquery.toolbar.css",
            allFrames: true
          });
          chrome.tabs.insertCSS({
            file: "/assets/reminder.css",
            allFrames: true
          });
        }
      );

      chrome.tabs.executeScript({
        file: "./js/external/lodash.js"
      });

      chrome.tabs.executeScript({
        file: "/js/inject.js"
      });
    }
  );
  return false;
};

function registerWordIntoDB(text) {
  let src, target, fl, regexp_forSearch;

  if (translateTo === "Korean") {
    src = "en";
    target = "kr";
    fl = "se";
  } else if (translateTo === "English") {
    src = "kr";
    target = "en";
    fl = "sk";
  }

  $.ajax({
    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
    headers: {
      Authorization: "KakaoAK 9ccdc05e28d13f3362fc84f11e38fb46"
    },
    data: {
      src_lang: src,
      target_lang: target,
      query: text
    },
    method: "POST",
    url: "https://kapi.kakao.com/v1/translation/translate",
    complete: function(result) {
      let en, kr;
      const translatedText = result.responseJSON.translated_text[0][0];

      if (fl === "se") {
        en = text;
        kr = [translatedText];
      } else if (fl === "sk") {
        en = translatedText;
        kr = [text];
      }

      regexp_forSearch = [
        "(?<=[^가-힣0-9])" + kr + "(?=[^가-힣])",
        "(?<=[^a-zA-Z0-9])" + en + "(?=[^a-zA-Z0-9])"
      ];

      createModalAtWindow(translatedText);

      const dataset = {
        index: Date.now(),
        eng: en,
        kor: kr,
        flag: fl,
        regexr: regexp_forSearch,
        matchedCount: 0
      };
      fireStore.collection("externalRegistered").add(dataset);
    }
  });
}

function createModalAtWindow(text) {
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {
      id: "createModal",
      text: text
    });
  });
}

function registerException(url) {
  const urlWithoutHead =
    url.indexOf("https") === 0 ? url.substring(8) : url.substring(7);
  const urlRepresented = urlWithoutHead.split("/")[0];
  const escapedURL = _.escapeRegExp(urlRepresented).replace("/", "\\/");
  exceptions.push(new RegExp(escapedURL));
  exceptions = _.uniqWith(exceptions, _.isEqual);

  window.localStorage.setItem("exceptions", exceptions);

  return false;
}

function changeLevel(lv) {
  let dataset_name;

  chrome.storage.local.set({ currentLevel: lv });

  switch (lv) {
    case "Basic": {
      dataset_name = "words.csv";
      break;
    }
    case "Advenced": {
      dataset_name = "word2.csv";
      break;
    }
    case "Expert": {
      dataset_name = "word3.csv";
      break;
    }
  }

  return dataset_name;
}

function storeInRemindPool(target) {
  const targetObj = _.chain(unknownWords)
    .find({ eng: target.trim() })
    .cloneDeep()
    .value();
  const dbState = db
    .get("remindPool")
    .push(targetObj)
    .unionWith(_.isEqual)
    .value();
  db.set("remindPool", dbState).write();
}

function removeItemFromRemindlists(target) {
  db.get("remindPool")
    .remove({ eng: target.trim() })
    .write();
}

// dec2hex :: Integer -> String
function dec2hex(dec) {
  return ("0" + dec.toString(16)).substr(-2);
}

// generateId :: Integer -> String
function generateId(len) {
  var arr = new Uint8Array((len || 40) / 2);
  window.crypto.getRandomValues(arr);
  return Array.from(arr, dec2hex).join("");
}

function init_database() {
  const lev_dataset = changeLevel(currentLv);
  chrome.storage.local.set({ currentLevel: currentLv });

  db.setState({});

  db.defaults({
    original: [],
    unknown: [],
    known: [],
    common: [],
    remindPool: []
  }).write();

  const userID = generateId(20);
  window.localStorage.setItem("userid", userID);

  fireStorage = firebase.storage();
  fireStore = firebase.firestore();

  const storageRef = fireStorage.ref(lev_dataset);
  storageRef.getDownloadURL().then(url => {
    Papa.parse(url, {
      delimiter: ",",
      download: true,
      complete: function(result) {
        (() => {
          const data = result.data;
          const store = window.indexedDB.open("dbs", revisionNum);
          store.onupgradeneeded = event => {
            const idb = event.target.result;
            let objStore = idb

            if (idb.objectStoreNames.length !== 0) {
              idb.deleteObjectStore("unknown")
            } 
            
            objStore = idb.createObjectStore("unknown", {
              keyPath: "index"
            });
            objStore.createIndex("index", "index", { unique: true });  
          

            objStore.transaction.oncomplete = function(event) {
              const customerObjectStore = idb
                .transaction("unknown", "readwrite")
                .objectStore("unknown");
                
              for (let i = 0; i < data.length; i++) {
                customerObjectStore.add({
                  index: data[i][0],
                  eng: data[i][1],
                  kor: data[i][2] != "" ? data[i][2].split(";") : null,
                  flag: data[i][3],
                  pron: data[i][4],
                  class: data[i][5],
                  orikor: data[i][6] != "" ? data[i][6].split(";") : null,
                  regexr: getRegexr(data[i][2].split(";"), data[i][3]),
                  matchedcount: 0
                });
              }
              window.localStorage.setItem("totalLen", data.length);
            };
          };
          store.onsuccess = event => {
            const idb = event.target.result;

            const objStore = idb.transaction("unknown").objectStore("unknown");
            unknownWords = [];

            objStore.openCursor().onsuccess = event => {
              const cursor = event.target.result;
              if (cursor) {
                const item = cursor.value;
                unknownWords.push(item);

                cursor.continue();
              }
            };
          };
        })();
      }
    });
  });
}

function openReminderPage() {
  chrome.tabs.executeScript({
    code: "openRightDrawer()",
    allFrames: true,
    runAt: "document_end"
  });
  return false;
}

function changeWordLimit(num) {
  limitWords = num;
  chrome.storage.local.set({ wordLimit: num });
  return false;
}

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, Tab) {
  if (changeInfo.status === "complete") {
    const userId = window.localStorage.getItem("userid");
    if (!userId || isPauseStatus) {
      return false;
    }

    injectScriptsToContentsScript();
  }
});

chrome.runtime.onMessage.addListener(function(req, sender, sendResponce) {
  if (req.id === "init") {
    initContextMenu();
    checkAvailable();
  } else if (req.id === "matchedCount") {
    countUpMatchedWord(req.targetWord);
  } else if (req.id === "pass") {
    moveWordToKnown(req.targetWord);
  } else if (req.id === "pauseApp") {
    pauseApp();
  } else if (req.id === "refreshDB") {
    refreshDb();
  } else if (req.id === "releaseApp") {
    releaseApp();
  } else if (req.id === "translateTo") {
    translateTo = req.val;
    chrome.storage.local.set({ translateTo: translateTo });
  } else if (req.id === "changeLv") {
    currentLv = req.lv;
    revisionNum += 1;
    init_database();
  } else if (req.id === "remindAgain") {
    storeInRemindPool(req.targetWord);
  } else if (req.id === "moveToReminder") {
    openReminderPage();
  } else if (req.id === "changeWordLimit") {
    changeWordLimit(req.val);
  } else if (req.id === "deleteItemFromRemindLists") {
    removeItemFromRemindlists(req.target);
  }

  function checkAvailable() {
    const url = req.url;
    const blogURLPattern = /(https?:\/\/)blog.naver.com\/\w*/;
    let result;

    // if (!enableLicence) {
    //   return false;
    // }

    if (blogURLPattern.test(url)) {
      isNaverblog = true;
    } else {
      for (let i = 0; i < exceptions.length; i++) {
        if (exceptions[i].test(url)) {
          return false;
        }
      }
      isNaverblog = false;
    }

    chrome.tabs.detectLanguage(function(lan) {
      if (translateTo === "English") {
        result = lan === "ko" ? true : false;
      } else if (translateTo === "Korean") {
        result = lan === "en" ? true : false;
      }

      if (result || isNaverblog) {
        transferWordsToContentScript();
      }
    });
  }
  function checkExceptedPage(callback) {
    let result = false;

    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      for (let i = 0; i < exceptions.length; i++) {
        if (exceptions[i].test(tabs[0].url)) {
          result = true;
        }
      }

      if (typeof callback === "function") {
        callback(result);
      }
    });
  }

  function initContextMenu() {
    clientId = "zh3R64DtV4Gxobe5Tdhk";
    clientSecret = "8APUbTPx0Y";

    checkExceptedPage(function(result) {
      const contextMenuTitle = result
        ? "Release Exception"
        : "Except This Page";
      chrome.contextMenus.removeAll(() => {
        chrome.contextMenus.create({
          id: "registerItem",
          title: "search a meaning",
          contexts: ["selection"]
        });
        chrome.contextMenus.create({
          id: "exception",
          title: contextMenuTitle,
          contexts: ["page"]
        });

        chrome.contextMenus.onClicked.addListener(info => {
          if (info.menuItemId === "registerItem") {
            registerWordIntoDB(info.selectionText);
          } else if (info.menuItemId === "exception") {
            registerException(info.pageUrl);
          }

          return false;
        });
      });
    });
  }

  function countUpMatchedWord(target) {
    const db_instant = unknownWords;
    const wordCount = db_instant.find({ eng: target.trim() }).value()
      .matchedcount;

    if (wordCount > 5) {
      moveWordToKnown(target);
    } else {
      _.chain(db_instant)
        .find({ eng: target.trim() })
        .assign({ matchedcount: wordCount + 1 })
        .write();
    }
  }

  function moveWordToKnown(target) {
    const db_unknown = unknownWords;
    const db_known = db.get("known");

    const valueOfFoundFromUnknownDB = _.chain(db_unknown)
      .find({ eng: target.trim() })
      .cloneDeep()
      .value();
    const isExistInKnownDB =
      db_known.find({ index: valueOfFoundFromUnknownDB.index }).value() !==
      undefined;
    if (!isExistInKnownDB) {
      db_known.push(valueOfFoundFromUnknownDB).write();
      _.remove(db_unknown, { index: valueOfFoundFromUnknownDB.index });
    }
  }

  function pauseApp() {
    isPauseStatus = true;
    return false;
  }

  function refreshDb() {
    init_database();
  }

  function releaseApp() {
    isPauseStatus = false;
    return false;
  }
  return false;
});
