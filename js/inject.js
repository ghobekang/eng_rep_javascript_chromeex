function replaceAll(strTemp, strValue1, strValue2){ 
    while(1){
        if( strTemp.indexOf(strValue1) != -1 )
            strTemp = strTemp.replace(strValue1, strValue2);
        else
            break;
    }
    return strTemp;
}

function unicodeEnc(text) {
    return escape(replaceAll(text, "\\", "%"));
}

function unicodeDec(text) {
    return unescape(replaceAll(text, "\\", "%"));
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function showupModal(text) {
    const body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');

    const modal = `<div class="searchResultModal">
    <div class="modal_wrap"><span class="close_btn">x</span><p>${text}</p></div></div>`;

    body.append(modal);
}
function closeModal() {
    const body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');

    body.find('.searchResultModal').remove();
}

function splashStart() {
    let body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');

    const dimLayer = 
        `<div class="splash">
            <div class="loading_wrap"><span>Analyzing...</span></div>
        </div>`;
    
    body.append(dimLayer);
}
function splashEnd() {
    let body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');

    body.find('.splash').remove();
}

function checkURL() {
    let result = 'co';
    const currentURL = window.location.href;
    const blogURLPattern = /(https?:\/\/)blog.naver.com\/\w*/;
    const postURLPattern = /(https?:\/\/)post.naver.com\/\w*/;
    const newsURLpattern = /(https?:\/\/)news.naver.com\/\w*/;
    const cafeURLpattern = /(https?:\/\/)cafe.naver.com\/\w*/;
    const mailURLpattern = /(https?:\/\/)mail.naver.com\/\w*/;

    if (blogURLPattern.test(currentURL)) {
        result = 'blog';
    } else if (postURLPattern.test(currentURL)) {
        result = 'post';
    } else if (newsURLpattern.test(currentURL)) {
        result = 'news';
    } else if (cafeURLpattern.test(currentURL)) {
        result = 'cafe';
    } else if (mailURLpattern.test(currentURL)) {
        result = 'mail';
    }

    return result;
}

function checkNumOfLoop() {
    const typeOfURL = checkURL();
    if (typeOfURL === 'post' ||
        typeOfURL === 'cafe' ||
        typeOfURL === 'mail') {
            return 1;
    } else {
        return 2;
    }
}

function checkTransStatus() {
    chrome.storage.local.get(['translateTo'], function(result) {
        if (result.translateTo) {
            return result.translateTo;
        }
    });
}

function getSearchStartPoint(type) {
    let result;

    switch (type) {
        case 'blog' : {
            result = '.se-viewer';
            break;
        }
        case 'post' : {
            result = '.se_component_wrap';
            break;
        }
        case 'news' : {
            result = '.article_body';
            break;
        }
        case 'co' : {
            result = '*';
            break;
        }
    }
    
    return result;
}

function getOriginWordByTranslate(target) {
    $.ajax({
        async: false,
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
        headers : {
          Authorization: 'KakaoAK 9ccdc05e28d13f3362fc84f11e38fb46'
        },
        data: {
          src_lang: 'en',
          target_lang: 'kr',
          query: target
        },
        method: 'POST',
        url: 'https://kapi.kakao.com/v1/translation/translate',
        complete: function(result) {
          return result.responseJSON.translated_text[0][0];
        }
      });
}

function getWordColor(type) {
    let result;

    switch (type) {
        case 'i': {
            result = '#68a6fb';   
            break;
        }
        case 'v': {
            result = '#ffc379';
            break;
        }
        case 'vp': {
            result = '#ff7979';
            break;
        }
        case 'a': {
            result = '#fb6868';
            break;
        }
        case 'n': {
            result = '#a168fb';
            break;
        }
        case 'd': {
            result = '#81d200';
            break;
        }
        case 't': {
            result = '#002646';
            break;
        }
    }
    return result;
}

function getWordsLimit() {
    return 
}

async function replaceWordsOfHTML(wordList, isNaverBlog, translateTo,isUnknown, limitWords, callback) {
    function termination() {
        if (typeof callback === 'function') {
            callback(isNaverBlog, translateTo);
        }
        storePaging(wordPaging);
        if (checkURL() === 'news') {
            $('.reminder').click(function (ev) {
                if (ev.isPropagationStopped()) {
                    return;
                }
                chrome.runtime.sendMessage({
                    id: 'moveToReminder'
                });
                ev.stopPropagation();
            });
        }
        return false;
    }

    let limitLoops = 400;
    
    let wordPaging = JSON.parse(window.localStorage.getItem('paging'));
    if (!wordPaging) wordPaging = [];

    for (let i=0; i<wordList.length; i++) {
        if (window.wordsCount > limitWords) {
            termination();
            return false;
        }
        if (i > limitLoops) {
            termination();
            return false;
        }

        await sleep(0);

        getSampleSentences(wordList[i], translateTo, function(samples, index) {
            findTargetObjects(samples, checkURL(),function(matchedSentence) {
                let replacedWord, transWord, testRegExp;
                try {                 
                    transWord = translateTo === 'Korean' ? wordList[i].orikor[0] : wordList[i].eng;
                    testRegExp = translateTo === 'Korean' ? new RegExp('(?<=\\s)'+ wordList[i].eng + '(?=\\s)', 'g'): new RegExp(wordList[i].regexr[index], 'g');
                } catch (e) {
                    console.log(e.message);
                }

                let innerWord = matchedSentence[0].innerHTML.match(testRegExp);
                if (innerWord) {
                    innerWord = unicodeEnc(innerWord[0]);
                }

                if (isUnknown) {
                    const eng = wordList[i].eng;
                    const kor = unicodeEnc(wordList[i].kor[index]);
                    const class_ = wordList[i].class !== undefined ? wordList[i].class : '';
                    const orikor = wordList[i].orikor !== undefined ? unicodeEnc(wordList[i].orikor[index]) : '';
                    const pron = wordList[i].pron !== undefined ? wordList[i].pron : '';
                    const wordColor = getWordColor(wordList[i].flag);
                    const entireMeans = wordList[i].orikor !== undefined ? unicodeEnc(wordList[i].orikor) : '';

                    replacedWord = '<a class="replacedWord_" data-entire="'+entireMeans+'" data-innerword="'+innerWord+'" data-eng="'+eng+'" data-kor="'+kor+'" data-class="'+class_+'" data-orikor="'+orikor+'" data-pron="'+pron+'"><span style="color: black; border-bottom: solid 3px '+wordColor+'">'+ transWord +'</span></a>'; 
                } else {
                    replacedWord = transWord;
                }

                if (testRegExp.test(matchedSentence[0].innerHTML)) {
                    
                    try {
                        matchedSentence[0].innerHTML = 
                        matchedSentence[0].innerHTML.replace(testRegExp, replacedWord);
                    } catch (err) {
                        console.log(err);
                    }
                    
                    isRow = false;
                    window.wordsCount += 1;
                   
                    wordPaging.push(wordList[i]);
                }
            });
        });
    }
}

function storePaging(wordPaging) {
    const metchedWordsBefore = JSON.parse(window.localStorage.getItem('paging')) ?
                                        JSON.parse(window.localStorage.getItem('paging')) : [];
    const metchedWords = _.chain(_.concat(metchedWordsBefore, wordPaging)).unionWith(_.isEqual).takeRight(100);
    window.localStorage.setItem('paging', JSON.stringify(metchedWords));
}

function getSampleSentences(wordObj, translateTo, callback) {
    let len = wordObj.kor.length;
    let body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');
    let sampleStringBeforeTransforming;
    let index;
    
    for (let i=0; i < len; i++) {
        if (wordObj.kor[i] === '') {
            continue;
        }
            
        if (translateTo === 'Korean') {
            sampleStringBeforeTransforming = [wordObj.eng];
        } else if (translateTo === 'English') {
            if (wordObj.flag === 'a' || wordObj.flag === 'v') {
                sampleStringBeforeTransforming = body.html().match(new RegExp(wordObj.kor[i],'g'));
                if (!sampleStringBeforeTransforming) {
                    continue;
                } else {
                    if (typeof callback === 'function') {
                        index = i;
                        callback(_.unionWith(sampleStringBeforeTransforming, _.isEqual), index);
                    }
                }
            } else {
                sampleStringBeforeTransforming = [wordObj.kor[i]];
                if (typeof callback === 'function') {
                    index = i;
                    callback(sampleStringBeforeTransforming, index);
                }
            }
        }
    }
    return false;
}


function findTargetObjects(matchedStrings_arr, exceptionType, callback) {
    let searchStartPoint = getSearchStartPoint(checkURL());
    const len = matchedStrings_arr.length;
    let body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');
    const exceptions = getExceptions(exceptionType);

    for (let i=0; i < len; i++) {
        let matchedSentence;

        if (checkURL() !== 'co') {
            matchedSentence = body.find(searchStartPoint).find('*:contains('+ matchedStrings_arr[i] +')').not(exceptions).last();
        } else {
            matchedSentence = body.find('*:contains('+ matchedStrings_arr[i] +')').not(exceptions).last();    
        }
        if (matchedSentence.length !== 0) {
            if (typeof callback === 'function') {
                callback(matchedSentence);
            }
            return false;
        }
    }
}
function getExceptions(flag) {
    let exception = '*:has("a.replacedWord_"), *:has("span.reminder"), .tool-container, .tool-container *, .replacedWord_, .replacedWord_ *, script, #header, #header *, #header_wrap, #header_wrap *, a, img, link, button, i';

    if (flag !== 'co' && flag !== 'news') {
        exception = '*:has("a.replacedWord_"), *:has("span.reminder"), .tool-container, .tool-container *, .replacedWord_, .replacedWord_ *, script, a, img, link, button, i';
    }
    if (flag === 're') {
        exception = '.tool-container, .tool-container *, .replacedWord_, .replacedWord_ *, script, img, link, button';
    }
    if (flag === 'news') {
        exception = '.tool-container, .tool-container *, script, a, img, link, button, i';
    }

    return exception;
}
async function createToolbars(isNaverBlog, translateTo) {

    let targetElements = $('.replacedWord_');
    if (isNaverBlog) {
        targetElements = $(window.frames[0].document.body).find('#postListBody .replacedWord_');
    }

    if (targetElements.length !== 0) {
        for (let i=0; i<targetElements.length; i++) {
            
            await sleep(0);

            let mainWord, subWord, pronounce, explaneSentence, wordclass, originkor;
            let calculatePostion = 'top';
            let positionElement = $(targetElements[i]).offset().top;
            if (positionElement < 350) {
                calculatePostion = 'bottom';
            }
            if (translateTo === 'Korean') {
                mainWord = $(targetElements[i]).attr('data-innerword');
                subWord = $(targetElements[i]).attr('data-eng');
                pronounce = $(targetElements[i]).attr('data-pron');
                wordclass = $(targetElements[i]).attr('data-class');
                originkor = '';
                explaneSentence = 'Can you think this word\'s meaning out within 3 sec?';
            } else if (translateTo === 'English') {
                mainWord = $(targetElements[i]).attr('data-eng');
                subWord = unicodeDec($(targetElements[i]).attr('data-innerword'));
                pronounce = $(targetElements[i]).attr('data-pron');
                originkor = unicodeDec($(targetElements[i]).attr('data-orikor'));
                wordclass = $(targetElements[i]).attr('data-class');
                entireMeans = unicodeDec($(targetElements[i]).data('entire'));
                explaneSentence = '이 단어를 보고 3초 이내에 뜻이 생각이 났나요?';
            }

            const template = 
            `   <div class="wordMeaning">
                    <div class="wordEng_wrap">
                        <div class="wordEng">
                            <span>${mainWord}</span>
                        </div>
                        <div class="wordPronounce">
                            <sapn>[${pronounce}], [${wordclass}]</sapn>
                        </div>    
                    </div>
                    <div class="wordKor">
                        <span class="innerword">${originkor}</span>
                        <span class="originword">(${subWord})</span>
                    </div>
                </div>
                <div class="moreMeaning_wrap">
                    <button id="more_btn" class="moreSee">
                        <span>Seeing More <strong style="color:rgb(139, 178, 190);">M</strong>eanings</span>
                    </button>
                    <div class="moreMeanings">${entireMeans}</div>
                </div>
                <div class="distinguishKnownWord">
                    <button class="applyKnown">
                        <span><strong style="color:rgb(139, 178, 190);">P</strong>ASS</span>
                    </button>
                    <div class="questionKnown">
                        <span>Already know it?</span>
                    </div>
                </div>
                <div class="q-mark"></div>
                <div class="questionWording">
                    <p style="margin:0">${explaneSentence}</p>
                </div>
                <div class="btnGroup_Question">
                    <button class="btn_reject">No</button>
                    <button class="btn_apply">Yes</button>
                </div>`;

            $(targetElements[i]).toolbar({
                content: template,
                position: calculatePostion,
                animation: 'bounce',
                event: 'click',
                hideOnClick: true,
                adjustment: 355
            });
        } 
    }

    if (isNaverBlog) {
        $(window.frames[0].document.body).find('.tool-container').each(function(index, element) {
            registerListeners.call(this, index);
        });
    } else {
        $('.tool-container').each(function(index, element) {
            registerListeners.call(this, index);
        });
    }

    function registerListeners(index) {
        const self = this;
        const target = $(this).find('.wordEng').text();
        $(this).find('.applyKnown').click(function(ev) {
            chrome.runtime.sendMessage({
                id: 'pass',
                targetWord: target
            }, function() {
                $('.replacedWord_').eq(index).addClass('disabled');
            });
            close();
        });
        $(this).find('.btn_apply').click(function(ev) {
            chrome.runtime.sendMessage({
                id: 'matchedCount',
                targetWord: target
            });
            close();
        });
        $(this).find('.btn_reject').click(function(ev) {
            chrome.runtime.sendMessage({
                id: 'remindAgain',
                targetWord: target
            });
            close();
        });
        $(this).find('.moreSee').click(function(ev) {
            if (!ev.isPropagationStopped()) {
                $(ev.currentTarget).parent().find('.moreMeanings').toggleClass('active');
            }
            ev.stopPropagation();
        });

        function close() {
            $(self).css({opacity: '0', display: 'none'});
        }
    }
}

function manipulateRemindWords(remindWords, isNaverBlog, translateTo) {
    const len = remindWords.length;
    let body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');

    if (remindWords) {
        appendDrawer_right(remindWords, translateTo);
    }

    for (let i=0; i < len ; i++) {
        getSampleSentences(remindWords[i], translateTo, function(samples, index) {
            findTargetObjects(samples, 're',function(matchedSentence) {
                const testRegExp = translateTo === 'Korean' ? 
                    new RegExp('(?<=\\s)'+ remindWords[i].eng + '(?=\\s)', 'g') 
                    : new RegExp(remindWords[i].regexr[index],'g');

                let targetWord = matchedSentence.html().match(testRegExp);
                if (!targetWord) {
                    targetWord = '';
                } else {
                    targetWord = targetWord[0];
                }
                const replaceElement = '<span class="reminder" style="color: red;">'+targetWord+'</span>';
                matchedSentence[0].innerHTML = matchedSentence[0].innerHTML.replace(testRegExp, replaceElement);
            });
        });
    }
    body.find('.reminder').click(function (ev) {
        if (ev.isPropagationStopped()) {
            return;
        }
        chrome.runtime.sendMessage({
            id: 'moveToReminder'
        });
        ev.stopPropagation();
    });
}

function appendDrawer_right(words, translateTo) {
    let body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');
    const template_drawer = 
    `<div id="drawer_wrap">
        <h2>Voca</h2>
        <div id="edit_voca">Edit</div>
        <ul class="remind_rightDrawer"></ul>
    </div>`;

    body.append(template_drawer);
    const appendPosition = body.find('.remind_rightDrawer');

    if (words) {
        const len = words.length;
        for (let i=0; i<len; i++) {
            const remindWord = translateTo === 'English' ? words[i].eng : words[i].kor[0];
            const wordClass = words[i].class;
            const meaning = translateTo === 'English' ? words[i].orikor : words[i].eng;
            const template = 
            `<li class="remind_item">
                <div class="remind_word">
                    <span>${remindWord}</span>
                    <button class="delItemFromRemind">Delete</button>
                </div>
                <div class="remind_infos">
                    <span class="wordClass">[${wordClass}]</span>
                    <span class="wordMeaning">[${meaning}]</span>
                </div>
            </li>`;
            appendPosition.append(template);
        }
        body.find('#edit_voca').click(function(ev) {
            body.find('.delItemFromRemind').toggleClass('editable');
        });
        body.find('.delItemFromRemind').click(function(ev) {
            chrome.runtime.sendMessage({
                id: "deleteItemFromRemindLists",
                target: $(ev.currentTarget).siblings('span').text()
            });
            $(ev.currentTarget).parent().remove();
        })
        body.find('.remind_word').click(function(ev) {
            ev.stopPropagation();
            $(ev.currentTarget).parent().find('.remind_infos').toggleClass('active');
        });
    }
}

function openRightDrawer() {
    let body = checkURL() === 'blog' ? $(window.frames[0].document.body) : $('body');
    body.find('#drawer_wrap').addClass('active_drawer').removeClass('closed_drawer');
    body.find('*').not('#drawer_wrap, #drawer_wrap *').click(function(ev) {
        if (body.find('#drawer_wrap').hasClass('active_drawer')) {
            body.find('#drawer_wrap').addClass('closed_drawer').removeClass('active_drawer');
        }
    });
}


chrome.runtime.sendMessage({ id: 'init', data: document.body.innerText, url: window.location.href });

chrome.runtime.onMessage.addListener(
    function(req, sender, sendResponce) {

        if (req.id === 'createModal') {
            showupModal(req.text);

            body.find('.searchResultModal .close_btn').click(function() {
                closeModal();
            });

            return false;
        } else {
            window.wordsCount = 0;
            const translateTo = req.trans;
            const isNaverBlog = req.isNaverBlog;
    
            let regexrWords = req.regexrWords;
            const knownWords = req.known;
            const commonSentences = req.common;
            const remindWords = req.reminds;
            const limitWords = req.limit;
            const pagingWords = JSON.parse(window.localStorage.getItem('paging'));
            
            if (remindWords) {
                manipulateRemindWords(remindWords, isNaverBlog, translateTo);
            }
            if (pagingWords) {
                replaceWordsOfHTML(pagingWords, isNaverBlog, translateTo, true, limitWords, createToolbars);
            }
           
            if (commonSentences.length !== 0) {
                regexrWords = commonSentences.concat(regexrWords);
            }

            const numberOfLoop = checkNumOfLoop();
            for (let i=0; i<numberOfLoop; i++) {
                replaceWordsOfHTML(_.shuffle(regexrWords), isNaverBlog, translateTo, true, limitWords, createToolbars);
            }

            if(knownWords.length !== 0) {
                replaceWordsOfHTML(knownWords, isNaverBlog, translateTo, false, createToolbars);
            }
                
            return false;
        }
        
    }
);