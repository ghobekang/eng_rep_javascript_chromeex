let unknowndb, knowndb, origindb;
let knownWords = [];
let licenses;

$(document).ready(function() {
    google.payments.inapp.getPurchases({
        'parameters': {env: 'prod'},
        'success': function(response) {
            licenses = response.response.details;
        },
        'failure': function(response) {
            const err = response.response.errorType;
        }
    });

    $('.unknownWordsStatus > span:nth-child(2)').text(JSON.parse(window.localStorage.getItem('db')).unknown.length + '개');
    $('.knownWordsStatus > span:nth-child(2)').text(JSON.parse(window.localStorage.getItem('db')).known.length + '개');
    knownWords = JSON.parse(window.localStorage.getItem('db')).known;
    
    chrome.storage.local.get(['isPaused', 'translateTo', 'currentLevel', 'wordLimit'], function(result) {
        if (result.isPaused) {
            $('#turn_off').attr('checked', true);
        }
        if (result.translateTo) {
            $('#translateTo').find('option:contains('+ result.translateTo +')').attr('selected', true);
        }
        if (result.currentLevel) {
            $('.levelStates_btngroup > .selected').removeClass('selected');
            $('.levelStates_btngroup > button:contains('+result.currentLevel+')').addClass('selected');
        }
        if (result.wordLimit) {
            let targetBtn;
            targetBtn = result.wordLimit === 400 ? 'Infinity' : result.wordLimit; 
            $('.limit_btnGrp').find('button:contains('+ targetBtn +')').addClass('selected');
        }
    });

    const exceptions = window.localStorage.getItem('exceptions').split(',');
    if (exceptions !== undefined) {
        for (let i=0; i<exceptions.length; i++) {
            const exceptionItem = '<li class="exceptionItem"><span>'+exceptions[i].replace(/\\\./g, '.').replace(/(^\/|\/$)/g, '')+'</span></li>';
            $('.exceptionLists > ul').append(exceptionItem);
        }
    }
    const appendKnownWords = new Promise(function(resolve, reject) {
        for (let i=0; i<knownWords.length; i++) {
            $('.flashcards').append('<div class="flashcard"><div class="flashcard_eng"><span>'+knownWords[i].eng+'</span></div><div class="flashcard_pron"><span>'+knownWords[i].pronounce+'</span></div></div>')
        }
        resolve();  
    });

    appendKnownWords.then(function() {
        $('.flashcards').slick({
            autoplay: true,
            prevArrow: $('.prev_arrow'),
            nextArrow: $('.next_arrow')
        });    
    });
  
});

$('#turn_off').change(function(ev) {
    const isChecked = ev.target.checked;
    
    if(isChecked) {
        chrome.storage.local.set({'isPaused': true});
        chrome.runtime.sendMessage({
            id: 'pauseApp'
        });
    } else {
        chrome.storage.local.set({'isPaused': false});
        chrome.runtime.sendMessage({
            id: 'releaseApp'
        })
    }
});
$('#refreshDB').click(function(ev) {
    chrome.runtime.sendMessage({
        id: 'refreshDB'
    });
});

$('#translateTo').change(function(ev){
    chrome.runtime.sendMessage({
        id: 'translateTo',
        val: $(this).find('option:selected').text()
    });
});

$('.limit_btnGrp > button').click(function (ev) {
    let numofLimit = $(this).text();
    if (numofLimit === 'Infinity') { numofLimit = 400;}
    chrome.runtime.sendMessage({
        id: 'changeWordLimit',
        val: parseInt(numofLimit)
    });

    $('.limit_btnGrp > .selected').toggleClass('selected');
    $(this).toggleClass('selected');
});

$('#language').change(function(ev) {
    chrome.runtime.sendMessage({
        id: 'language',
        val: $(this).find('option:selected').text()
    });
});

$('.startTest').click(function(ev) {
    $('.flashcard_wrap').toggleClass('active');
});

$('.mywords_btn').click(function() {
    chrome.runtime.sendMessage({
        id: 'moveToReminder'
    });
});

$('.exceptionLists_btn').click(function(ev) {
    $('.exceptionLists').toggleClass('active');
});

$('.levelStates_btngroup > button').click(function(ev) {
    if ($(this).text() === 'Expert') {
        const licenseState = checkLicense();
        if (!licenseState) {
            const sku = '';
            google.payments.inapp.buy({
                'parameters': {'env': 'prod'},
                'sku': sku,
                'success': onPurchase,
                'failure': onPurchaseFail
            });
            return false;
        }
    }
    $('.levelStatues_btngroup > .selected').toggleClass('selected');
    $(this).toggleClass('selected');
    
    const selectedLv = $(this).text();
    chrome.runtime.sendMessage({
        id: 'changeLv',
        lv: selectedLv
    });

    ev.stopPropagation();
});

function checkLicense() {
    for (let i=0; i<licenses.length; i++) {
        if (licenses[i].sku === 'test') {
            return true;
        }
    }

    return false;
}

function onPurchase(purchase) {
    window.alert('purchasing is successed');
}

function onPurchaseFail(purchase) {
    const reason = purchase.response.errorType;
    window.alert(reason);
}